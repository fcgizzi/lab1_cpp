# Contador de mayúsculas y minúsculas

    El programa conssite en un contador de mayusculas y minusculas de frases que se van ingresando en el arreglo

## Compilación
Se compila utilizando makefile
Ingresando en la terminal: `make` o también: `g++ ejercicio2.cpp Frases.cpp -o ejercicio2`

## Ejecución
Se ejecuta en terminal utilizando:
`./ejercicio2` y a este se le debe agregar un número para determinar el tamaño del arreglo

## Construido con:

- C++

- Geany
Se requiere una distribución de linux para poder visualizar el programa

## Autor

- Franco Cifuentes Gizzi (fcifuentes19@alumnos.utalca.cl)