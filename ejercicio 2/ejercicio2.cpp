/*
 * Compilación: $ make
 * Ejecución: $ ./ejercicio2 (y se le agrega el tamaño del arreglo))
 * se debe presionar enter para comenzara escribir las frases
 */
// librerias
#include <cctype>
#include <iostream>
using namespace std;

//clase 
#include "Frases.h"

// se inicia el programa con el tamaño del arreglo definido
int main(int argc, char **argv) {
	int n = atoi(argv[1]);
	
	// arreglo
	Frases frases[n];
	string frase;
	
	for(int i = -1; i < n; i++) {
		if(i >= 0) {
			cout << "Frase " << i + 1 << ": ";
			getline(cin, frase);
			// se guardan las frases
			frases[i].set_frase(frase);
			// se hace set a las frases para ver mayusculas y minusculas
			// también imprime
			cout << "Contiene: ";
			frases[i].set_mayusculas_count();
			frases[i].set_minusculas_count();
			cout << "---------------------------" << endl;
		}
		else{
			getline(cin, frase);
		}		
	}
	return 0;
}
