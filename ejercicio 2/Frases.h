#include <iostream>
using namespace std;

#ifndef FRASES_H
#define FRASES_H

class Frases {
    private:
        string frase = "\0";
	public:
        // constructores
        Frases();
        Frases (string frase);
        //metodos set y get
        void set_frase(string frase);
        void set_mayusculas_count();
        void set_minusculas_count();
        string get_frase();
};
#endif
