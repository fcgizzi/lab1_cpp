/*
 * Compilación: $ make
 * Ejecución: $ ./ejercicio3
 */
//libreria 
#include <iostream>
using namespace std;
//clase
#include "Cliente.h"

// función para imprimir totalidad de clientes con sus datos
 void imprimir(int i, Cliente cliente){
	cout << "Cliente " << i + 1 << endl;
	cout << "\t -Nombre: " << cliente.get_nombre() << endl;
	cout << "\t -Telefono: " << cliente.get_telefono() << endl;
	cout << "\t -Saldo: $" << cliente.get_saldo() << endl;
	// retorna true o false
	if(cliente.get_moroso()){
		cout << "\t -Morosidad: si" << endl;
	}
	else{
		cout << "\t -Morosidad: no" << endl;
	 }
}

// función para ingresar clientes al arreglo
void ingreso_clientes(int n){
	// variables y arreglo
	string nombre, telefono;
	int saldo, morosidad;
	bool moroso;
	Cliente clientes[n];
	// con ciclo for se van agragando a los set de la clase
	for(int i=0; i<n; i++){
		Cliente cliente = Cliente();
		cout << "Cliente número " << i + 1 << endl;

		cout << "\t Nombre: ";
		cin >> nombre;
		cliente.set_nombre(nombre);

		cout << "\t Teléfono: ";
		cin >> telefono;
		cliente.set_telefono(telefono);
		
		cout << "\t Saldo: ";
		cin >> saldo;
		cliente.set_saldo(saldo);
		
		cout << "\t El cliente es moroso? (1 = si | otro número = no): ";
		cin >> morosidad;
		
		if(morosidad == 1) {
			moroso = true;
		}
		else{
			moroso = false;
		}
		cliente.set_moroso(moroso);
		clientes[i] = cliente;
	}
	// se imprimen los clientes llamando a una función
	cout << endl << "-----------------------------------" << endl;
	cout << "Clientes en la empresa:" << endl;
	for(int i = 0; i < n; i++){
		imprimir(i, clientes[i]);
		cout << "-----------------------------------" << endl;
	}
}

//función principal
int main(int argc, char **argv){
	int n;
	cout << "Ingresar número de clientes: ";
	cin >> n;
	ingreso_clientes(n);

  return 0;
}



