// libreria
#include <iostream>
using namespace std;

//h
#include "Cliente.h"


// constructores
Cliente::Cliente(){
	string nombre = "\0";
	string telefono = "\0";
	int saldo = 0;
	bool moroso;
}

Cliente::Cliente(string nombre, string telefono, int saldo, bool moroso){
	this-> nombre = nombre;
	this-> telefono = telefono;
	this-> saldo = saldo;
	this-> moroso = moroso;
}

// metodos set y get
// cada set con su get

// nombre
void Cliente::set_nombre(string nombre){
	this-> nombre = nombre;
}
string Cliente::get_nombre(){
	return this-> nombre;
}

//teléfono
void Cliente::set_telefono(string telefono){
	this-> telefono = telefono;
}
string Cliente::get_telefono(){
	return this-> telefono;
}

// saldo
void Cliente::set_saldo(int saldo){
	this-> saldo = saldo;
}
int Cliente::get_saldo(){
	return this-> saldo;
}

//estado morosidad
void Cliente::set_moroso(bool moroso){
	this-> moroso = moroso;
}
bool Cliente::get_moroso(){
	return this-> moroso;
}
