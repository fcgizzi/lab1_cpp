# Clientes de empresa

    El programa consiste en una simulación de empresa, se ingresan clientes (con sus nombres, teléfonos, etc).
    Luego esta información es devuelta de forma ordenada

## Compilación
Se compila utilizando makefile
Ingresando en la terminal: `make` o también: `g++ ejercicio3.cpp Cuadrados.cpp -o ejercicio3`

## Ejecución
Se ejecuta en terminal utilizando:
`./ejercicio3` luego se le pide los datos

## Construido con:

- C++

- Geany
Se requiere una distribución de linux para poder visualizar el programa

## Autor

- Franco Cifuentes Gizzi (fcifuentes19@alumnos.utalca.cl)