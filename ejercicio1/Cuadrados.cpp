// clases
#include <iostream>
using namespace std;

#include "Cuadrados.h"

// constructores
Cuadrados::Cuadrados(){
  int cuadrado = 0;
}

Cuadrados::Cuadrados(int num){
  this->cuadrado = num * num;
}

void Cuadrados::set_cuadrado(int num){
  this->cuadrado = num * num;
}

// se obtiene el cuadrado del numero/objeto
int Cuadrados::get_cuadrado(){
  return this->cuadrado;
}
