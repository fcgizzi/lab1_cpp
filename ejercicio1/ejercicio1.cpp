/*
 * Compilación: $ make
 * Ejecución: $ ./ejercicio1 (y un número para definir tamaño de arreglo)
 */

#include <iostream>
using namespace std;
// clase para obtener los cuadrados de los números ingresados
#include "Cuadrados.h"

int main (int argc, char **argv) {
	int n = atoi(argv[1]);

	// se establece un arreglo para almacenar los números
	int numeros[n];
	// y uno para almacenar los cuadrados
	int cuadrados[n];
	
	// se ingresan los numeros mediante un ciclo for
	// se usa variable num como objeto
	int num;
	cout << "Ingresar números: "<< endl; 
	for(int i = 0; i < n; i++) {
		cout << i +1 << " : ";
		cin >> num;
		
		// se ingresa valor en el arreglo de números
		numeros[i] = num;
		
		// se llama a la clase
		Cuadrados cuadrado = Cuadrados();
		// se envia el numero a la clase
		cuadrado.set_cuadrado(num);
		// se ingresa el cuadrado en el arreglo de cuadrados
		cuadrados[i] = cuadrado.get_cuadrado();
	}
	// calcular suma de cuadrados
	int suma = 0;
	
	for(int i=0; i<n; i++){
		suma = suma + cuadrados[i];
	}
	// se imprimen los datos
	cout << "\t Resultados " << endl;
	
	for(int i=0; i<n; i++){
		cout << "Numero: " << numeros[i] << " ---> Cuadrado: " << cuadrados[i] << endl;
  }
	cout << "------------------------------"<< endl;
	cout << "\t\t  TOTAL: " << suma << endl;
  return 0;
}
