#include <iostream>
using namespace std;

#ifndef CUADRADOS_H
#define CUADRADOS_H

class Cuadrados {
    private:
        int cuadrado = 0;
	
	//constructores
    public:
        Cuadrados();
        Cuadrados(int num);

	//metodos 
        void set_cuadrado(int num);
        int get_cuadrado();
};
#endif
