# Calculadora de cuadrados

## Compilación
Se compila utilizando makefile
Ingresando en la terminal: `make` o también: `g++ ejercicio1.cpp Cuadrados.cpp -o ejercicio1`

## Ejecución
Se ejecuta en terminal utilizando:
`./ejercicio1` y a este se le debe agregar un número para determinar el tamaño del arreglo

## Construido con:

- C++

- Geany
Se requiere una distribución de linux para poder visualizar el programa

## Autor

- Franco Cifuentes Gizzi (fcifuentes19@alumnos.utalca.cl)